import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-itemview',
  templateUrl: './itemview.page.html',
  styleUrls: ['./itemview.page.scss'],
})
export class ItemviewPage implements OnInit {
  rate: any;
  location: any;
  link: any;
  // db = firebase.firestore();
  image1;
  description: any;
  images = [];
  len: any;
  data: any;
  phNo: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private callNumber: CallNumber,
    private share: SocialSharing) { }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
    // this.userId = this.route.snapshot.paramMap.get('data');
    // console.log('userId is :',this.userId)
    // this.ServicesService.getUserDetail(this.userId).then((res:any)=>{
    //   console.log(res)
    //   this.id = res;
    //   this.userdata = res;
    //   this.price = res.price;
    //   this.decription = res.decription;
    //   this.area = res.area;
    //   this.img = res.img;
    //   this.img2 = res.img2;
    //   this.img3 = res.img3;

    // this.loadingController.dismiss();
    // })
    this.route.params.subscribe(params => {
      this.data = params;
      this.rate = this.data.rate;
      this.description = this.data.description;
      this.location = this.data.location;
      this.len = this.data.imgLength;
      this.images = this.data.image1.split(',', this.len);
      this.phNo = this.data.phonenumber;
      console.log(this.phNo);
    });
    this.loadingController.dismiss();
  }
  callNow() {
    // tslint:disable-next-line: triple-equals
    if (this.phNo != null && this.phNo != undefined && this.phNo != '') {
      this.callNumber.callNumber(this.phNo, true)
        .then(res => {
          console.log('Launched dialer!', res);
        })
        .catch(err => {
          console.log('Error launching dialer', err);
        });
    }
  }

  sharing() {
    this.share.share(this.link).then(() => {
    }).catch(() => {

    });
  }

}
