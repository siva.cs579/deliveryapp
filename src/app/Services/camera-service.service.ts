import { Injectable } from '@angular/core';
import { Camera, CameraOptions} from '@ionic-native/camera/ngx';
import { ActionSheetController } from '@ionic/angular';
import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class CameraServiceService {

  constructor(private camera: Camera, public actionSheetCtrl: ActionSheetController) { }

  selectImage() {
    return new Promise(async (resolve) => {
      const actionSheet = await this.actionSheetCtrl.create({
        header: 'Select Image Source',
        buttons: [
          {
            text: 'Load from Library',
            handler: () => {
              this.openGallery().then((res) => {
                resolve(res);
              });
            }
          },
          {
            text: 'Use Camera',
            handler: () => {
              this.takePicture().then((res) => {
                resolve(res);
              });
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      await actionSheet.present();

    });

  }




  // Choosing an Image from Gallery
  openGallery() {
    // Defining camera options
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 480,
      allowEdit: true,
      correctOrientation: true
    };

    let base64ImageURL;

    return new Promise(resolve => {
      // tslint:disable-next-line: variable-name
      this.camera.getPicture(options).then((file_uri) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's file URI:
        base64ImageURL = 'data:image/jpeg;base64,' + file_uri;
        resolve(base64ImageURL);
      },
        (err) => {
          console.log(err);
        });
    });

  }

  // Choosing an Image using Camera
  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 800,
      targetHeight: 480,
      allowEdit: true,
      correctOrientation: true
    };

    let base64ImageURL;

    return new Promise(resolve => {
      this.camera.getPicture(options).then((imageData) => {
        // alert('in servi3')

        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        base64ImageURL = 'data:image/jpeg;base64,' + imageData;
        resolve(base64ImageURL);
      }, (err) => {
        // Handle error
        alert(err + ' er in capturing');
      });
    });
  }
}

