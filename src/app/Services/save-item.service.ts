import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class SaveItemService {

  constructor() { }

  createItemData(body) {
    // title: data.title,
    //   description: data.description,
    //   rate: data.rate,
    //   kgs: data.kgs,
    //   location: "kongavanipalem",
    //   district: "vzm",
    //   phonenumber: "8317587259",
    //   image: Images,
    //   date: this.date

    const db = firebase.firestore();
    const docRef = db.collection('UserList').doc();
    docRef.update({
      body
    }).then((doc) => {
      console.log(doc);
      alert('Updated Successfully');
    }).catch(err => {
      alert('Error While storing details' + err);
    });

  }
}
