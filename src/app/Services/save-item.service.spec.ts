import { TestBed } from '@angular/core/testing';

import { SaveItemService } from './save-item.service';

describe('SaveItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaveItemService = TestBed.get(SaveItemService);
    expect(service).toBeTruthy();
  });
});
