import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  sellItems: any[];
  val: any;
  constructor(public router: Router) {
    this.sellItems = ['Vegetables', 'Fruits', 'Meat', 'Milk', 'DriedFruits', 'Sweets'];
  }

addItem(item) {
  console.log('items selected is', item);
  this.val = item;
  this.router.navigate(['sellitem']);
}
}
