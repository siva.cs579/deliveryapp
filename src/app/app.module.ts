import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import * as firebase from 'firebase';

export const firebaseConfig = {
  apiKey: 'AIzaSyBml66LSvjfYr1NV-KSdsEze-FnD_Z7EfQ',
  authDomain: 'a-z-delivery.firebaseapp.com',
  databaseURL: 'https://a-z-delivery.firebaseio.com',
  projectId: 'a-z-delivery',
  storageBucket: 'a-z-delivery.appspot.com',
  messagingSenderId: '965888463616',
  appId: '1:965888463616:web:ddebba7f0d2f1ab2a119e5',
  measurementId: 'G-EWG9R9FN3Q'
};
firebase.initializeApp(firebaseConfig);
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    CallNumber,
    Camera,
    { provide: RouteReuseStrategy, 
      useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
