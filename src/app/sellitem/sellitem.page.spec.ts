import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SellitemPage } from './sellitem.page';

describe('SellitemPage', () => {
  let component: SellitemPage;
  let fixture: ComponentFixture<SellitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellitemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SellitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
