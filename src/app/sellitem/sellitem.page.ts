import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase';
import { CameraServiceService } from '../Services/camera-service.service';
import { SaveItemService } from '../Services/save-item.service';
import { Alert } from 'selenium-webdriver';
import { Storage } from '@ionic/storage';

const FirebaseImgUrls1: any = [];
let downloadURl;

@Component({
  selector: 'app-sellitem',
  templateUrl: './sellitem.page.html',
  styleUrls: ['./sellitem.page.scss']
})
export class SellitemPage implements OnInit {
  // tslint:disable-next-line: max-line-length
  img1 =
    'https://firebasestorage.googleapis.com/v0/b/miracledb-a1522.appspot.com/o/chock%203.jpg?alt=media&token=47fe688c-29ca-4ff2-9d00-a064656bbfce';
  // tslint:disable-next-line: max-line-length
  img2 =
    'https://firebasestorage.googleapis.com/v0/b/miracledb-a1522.appspot.com/o/chocklets.jpg?alt=media&token=f6fa3133-4ad5-4207-8fa6-578132387a5f';
  // tslint:disable-next-line: max-line-length
  img3 =
    'https://firebasestorage.googleapis.com/v0/b/miracledb-a1522.appspot.com/o/fruit3.jpg?alt=media&token=ec78a768-f92f-4aab-8f36-ba30510e9dea';

  itemImagesArray = [];
  itemImagesArray2 = [this.img1, this.img2, this.img3];
  private userForm: FormGroup;
  localStorage: Storage;
  phoneNo: any;
  date = new Date().toLocaleDateString();
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private camS: CameraServiceService,
    private saveItemDataS: SaveItemService,
    private storage: Storage
  ) {
    this.localStorage = this.storage;
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      // tslint:disable-next-line: max-line-length
      title: [
        '',
        Validators.compose([
          Validators.minLength(10),
          Validators.maxLength(30),
          Validators.pattern('[a-zA-Z0-9 ]*'),
          Validators.required
        ])
      ],
      // tslint:disable-next-line: max-line-length
      description: [
        '',
        Validators.compose([
          Validators.minLength(30),
          Validators.maxLength(80),
          Validators.pattern('[a-zA-Z0-9 ]*'),
          Validators.required
        ])
      ],
      rate: ['', Validators.required],
      kgs: ['', Validators.required]
    });
  }

  async openCam() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    await loading.present();
    this.loadingController.dismiss();
    this.camS
      .selectImage()
      .then(res => {
        this.itemImagesArray.push(res);
      })
      .catch(err => {
        this.loadingController.dismiss();
        alert('Error ' + err);
      });
  }

  removeImg(index: number) {
    if (index !== -1) {
      this.itemImagesArray.splice(index, 1);
    }
  }

  async createItem(data) {
    if (this.itemImagesArray.length > 0) {
      const loading = await this.loadingController.create({
        message: 'Please wait...'
      });
      await loading.present();
      this.uploadPicture1(data);
    } else {
      this.loadingController.dismiss();
      this.presentAlert();
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: '',
      subHeader: 'Image should not be empty',
      message: 'Please select image',
      buttons: ['OK']
    });
    await alert.present();
  }

  uploadPicture1(data) {
    this.itemImagesArray.forEach((item, index, array) => {
      this.imgUpload(item, data);
    });
  }

  async imgUpload(item, data) {
    const storageRef = firebase.storage().ref('/Item_images');
    const uuid = this.guid();
    const imageRef = storageRef.child('picmsg' + uuid);
    imageRef
      .putString(item, firebase.storage.StringFormat.DATA_URL)
      .then(snapshot => {
        const ref = imageRef.getDownloadURL();
        this.sendImgUrl(ref, data);
      })
      .catch(error => {
        alert(
          'Error Uploading I96mage: ' + JSON.stringify(error) + 'check ' + error
        );
      });
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return (
      s4() +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      '-' +
      s4() +
      s4() +
      s4()
    );
  }

  async sendImgUrl(a, data) {
    // show image on form to conform before creating event
    const imageLength = this.itemImagesArray.length;
    let fireLength;
    await a.then(url => {
      downloadURl = url;
      if (FirebaseImgUrls1.includes(downloadURl)) {
      } else {
        FirebaseImgUrls1.push(downloadURl);
        fireLength = FirebaseImgUrls1.length;
      }
    });
    if (imageLength === fireLength) {
      this.saveItemData(data, FirebaseImgUrls1);
    }
  }

  // tslint:disable-next-line: no-shadowed-variable
  async saveItemData(data, FirebaseImgUrls1) {
    this.localStorage.get('phone').then(val => {
      console.log(val);
      const phno = val;

      let Images;

      if (FirebaseImgUrls1 === undefined) {
        Images = [];
      } else {
        Images = FirebaseImgUrls1;
      }
      let result: any;
      const db = firebase.firestore();
      // const docredd = db.collection('UserList');
      // docredd
      //   .add({
      //     title: data.title,
      //     description: data.description,
      //     rate: data.rate,
      //     kgs: data.kgs,
      //     // location: 'kongavanipalem',
      //     // district: 'vzm',
      //     // phonenumber: '8142961250',
      //     image: Images,
      //     fav: false,
      //     date: this.date
      //   })
      //   .then(doc => {
      //     console.log(doc);
      //     this.successAlert();
      //     alert('Updated Successfully');
      //   })
      //   .catch(err => {
      //     this.failureAlert(err);
      //   });
      const docRef = db.collection('UserList').doc(`${phno}`);
      docRef
        .set({
          title: data.title,
          description: data.description,
          rate: data.rate,
          kgs: data.kgs,
          location: 'kongavanipalem',
          district: 'vzm',
          phonenumber: '8142961250',
          image: Images,
          fav: false,
          date: this.date
        })
        .then(doc => {
          console.log(doc);
          this.successAlert();
          alert('Updated Successfully');
        })
        .catch(err => {
          this.failureAlert(err);
        });
      this.loadingController.dismiss();
      this.userForm.reset();
    });
  }

  async failureAlert(err) {
    const alert = await this.alertController.create({
      header: '',
      subHeader: '',
      message: `Failed to save Data \n ${err}`,
      buttons: ['OK']
    });
    await alert.present();
  }

  async successAlert() {
    const alert = await this.alertController.create({
      header: '',
      subHeader: '',
      message: 'Data Saved Successfully',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigateByUrl('/tabs/explore');
          }
        }
      ]
    });
    await alert.present();
  }

  clearArray() {
    FirebaseImgUrls1.length = [];
  }
}
