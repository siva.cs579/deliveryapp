import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase';
import { LoadingController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
let lastVisible: any;
const imageurl: any = [];
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  myads = 'ads';
  db = firebase.firestore();
  last: any;
  items: any = [];
  localStorage: Storage;
  images: any;
  items1: firebase.firestore.DocumentData;
  boo: any;
  constructor(
    private loadingController: LoadingController,
    private router: Router,
    private toastController: ToastController,
    private storage: Storage) {
    this.getList();
    this.getFavItems();
    this.localStorage = this.storage;
  }

  ngOnInit() { }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }

  async getList() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();

    const teamMembers = [];
    await this.db.collection('UserList').get().then(data => {
      data.forEach(doc => {
        console.log(doc.data());
        this.items = doc.data();
        teamMembers.push(doc.data());
      });
      this.items = teamMembers;
    });
    this.loadingController.dismiss();
  }



  async like(fav) {
    this.localStorage.get('phone').then(val => {
      console.log(val);
      const phno = val;

      console.log(fav, 'like image');
      console.log(fav);
      const db = firebase.firestore();
      const docRef = db.collection('UserList').doc(`${phno}`);
      docRef.update({
        fav: true
      }).then((doc) => {
        console.log(doc);
        console.log('Profile Updated Successfully');
      }).catch(err => {
        alert('Error While updating Profile details' + err);
      });
    });

  }


  async unlike(boo) {
    this.localStorage.get('phone').then(val => {
      console.log(val);
      const phno = val;

      console.log(boo, 'unlike image');
      const db = firebase.firestore();
      const docRef = db.collection('UserList').doc(`${phno}`);
      docRef.update({
        fav: `${false}`
      }).then((doc) => {
        console.log(doc);
        alert('Profile Updated Successfully');
      }).catch(err => {
        alert('Error While updating Profile details' + err);
      });
    });

  }

  getFavItems() {
    const teamMembers = [];
    const db = firebase.firestore();
    db.collection('UserList').where('fav', '==', true).get()
      .then((querySnapshot) => {
        // tslint:disable-next-line: only-arrow-functions
        querySnapshot.forEach((doc) => {
          console.log(doc.data());
          this.items1 = doc.data();
          teamMembers.push(doc.data());
        });
        this.items1 = teamMembers;
      });

  }
}


