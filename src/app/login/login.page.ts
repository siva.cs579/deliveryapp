import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { LoadingController, AlertController } from '@ionic/angular';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  confirmationResult: firebase.auth.ConfirmationResult;
  codeResult: firebase.auth.ConfirmationResult;
  recaptcha: boolean;
  localStorage: Storage;
  otpval: any;
  open: boolean;
  countryCode = 'India +91';
  errormsg: string;
  loginform: FormGroup;
  verificationid: string;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private storage: Storage,
    private loadingController: LoadingController,
    private alertController: AlertController) {
    this.localStorage = this.storage;
  }

  ngOnInit() {
    this.loginform = this.fb.group({
      phone: ['', Validators.required],
      otp: ['', Validators.required]
    });
  }
  captchaVerifier() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      // tslint:disable-next-line: object-literal-key-quotes
      'size': 'invisible' // Don't want to display recaptcha use this
      // 'size': 'normal' // to display recaptch use this
    });
  }
  async sendotp() {
    const loading = await this.loadingController.create({
      message: 'OTP Sending...',
      showBackdrop: true,
      spinner: 'crescent'
    });

    await loading.present().then(() => {
      this.captchaVerifier();
      this.localStorage.set('phone', this.loginform.controls.phone.value);
      const phone = this.loginform.controls.phone.value;
      this.localStorage.set('phone', phone);
      const phonenumber = '+91' + this.loginform.controls.phone.value;
      this.open = !this.open;
      firebase.auth().signInWithPhoneNumber(phonenumber, this.recaptchaVerifier).then((result) => {
        this.confirmationResult = result;
        const resultval = result;
        this.codeResult = this.confirmationResult;
        const coderesult = this.codeResult;
        this.verificationid = this.codeResult.verificationId;
        console.log(this.verificationid);
        // alert(JSON.stringify(this.confirmationResult));
        // alert('Sms Sent succesfully');
        this.loadingController.dismiss();
        this.presentAlert(); // for alert message
        // this.router.navigate(['otp-verification', { code: verificationid, coderesult, resultval, phone }]);
      }).catch((err) => {
        console.log('Error Thrown:- ', err);
        alert('Error Thrown:- ' + err);
        this.loadingController.dismiss();
      });
    });
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'OTP Alert',
      subHeader: '',
      message: 'OTP sent Successfully',
      buttons: ['OK']
    });
    await alert.present();
  }

  verifyOtp() {
    console.log('verifi otp');
    const code = this.loginform.controls.otp.value;
    const credential = firebase.auth.PhoneAuthProvider.credential(this.confirmationResult.verificationId, code);
    firebase.auth().signInWithCredential(credential);
  }


  async presentAlertFailed(err) {
    const alert = await this.alertController.create({
      header: '',
      subHeader: 'Verification',
      message: `Verification failed \n\n${err}`,
      buttons: ['OK']
    });
    await alert.present();
  }

  async signIn(phoneNumber: number) {
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = '+91' + this.loginform.controls.phone.value;
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then(async confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        const prompt = await this.alertController.create({
          subHeader: 'Enter the Confirmation code',
          inputs: [{ name: 'confirmationCode', placeholder: 'Confirmation Code' }],
          buttons: [
            {
              text: 'Cancel',
              handler: data => { console.log('Cancel clicked'); }
            },
            {
              text: 'Send',
              handler: data => {
                confirmationResult.confirm(data.confirmationCode)
                  .then((result) => {
                    // User signed in successfully.
                    console.log(result.user);
                    // ...
                  }).catch((error) => {
                    // User couldn't sign in (bad verification code?)
                    // ...
                  });
              }
            }
          ]
        });
        await prompt.present();
      })
      .catch((error) => {
        console.error('SMS not sent', error);
      });
  }

  googleLogin() {
    // tslint:disable-next-line: prefer-const
    let provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider).then((result) => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      const token = result.credential;
      console.log(result);
      console.log(token);
      // The signed-in user info.
      const user = result.user;
      console.log(user.email);
      console.log(user.phoneNumber);
      console.log('success');
      this.router.navigate(['/tabs']);
      // ...
    }).catch((error) => {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      // The email of the user's account used.
      const email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      const credential = error.credential;
      // ...
    });
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider).then((result) => {
      // This gives you a Facebook Access Token. You can use it to access the Facebook API.
      const token = result.credential;
      // The signed-in user info.
      this.router.navigate(['/tabs']);
      console.log(result);
      console.log(token);
      // The signed-in user info.
      const user = result.user;
      console.log(user);
      console.log('success');
    }).catch((error) => {
      // Handle Errors here.
      if (error.code === 'auth/account-exists-with-different-credential') {
        const errorCode = error.code;
        console.log(errorCode);
        this.router.navigate(['/tabs']);
      } else {
        const errorMessage = error.message;
        console.log(errorMessage);
        // The email of the user's account used.
        const email = error.email;
        console.log(email);
        // The firebase.auth.AuthCredential type that was used.
        const credential = error.credential;
        console.log(credential);
        // ...
      }
    });
    // firebase.auth().signInWithRedirect(provider);
    // firebase.auth().getRedirectResult().then((result) => {
    //   if (result.credential) {
    //     // This gives you a Facebook Access Token. You can use it to access the Facebook API.
    //     var token = result.credential.accessToken;
    //     this.router.navigate(['/tabs']);
    //     // ...
    //   }
    //   // The signed-in user info.
    //   var user = result.user;
    // }).catch(function(error) {
    //   // Handle Errors here.
    //   var errorCode = error.code;
    //   var errorMessage = error.message;
    //   // The email of the user's account used.
    //   var email = error.email;
    //   // The firebase.auth.AuthCredential type that was used.
    //   var credential = error.credential;
    //   // ...
    // });
  }

  githubLogin() {
    const provider = new firebase.auth.GithubAuthProvider();
    firebase.auth().signInWithPopup(provider).then((result) => {
      // This gives you a GitHub Access Token. You can use it to access the GitHub API.
      const token = result.credential;
      // The signed-in user info.
      const user = result.user;
      console.log(user.email);
      this.router.navigate(['/tabs']);
      // ...
    }).catch((error) => {
      // Handle Errors here.
      const errorCode = error.code;
      console.log(errorCode);
      const errorMessage = error.message;
      console.log(errorMessage);
      // The email of the user's account used.
      const email = error.email;
      console.log(email);
      // The firebase.auth.AuthCredential type that was used.
      const credential = error.credential;
      console.log(credential);

      // ...
    });
  }
}
