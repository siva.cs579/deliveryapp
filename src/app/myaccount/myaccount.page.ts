import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {

  localStorage: Storage;
  username: any;
  image: any;
  constructor(private router: Router, private storage: Storage) {
    this.localStorage = this.storage;
  }

  ngOnInit() {
    this.getProfiledata();
  }

  getProfiledata() {
    this.localStorage.get('phone').then(val => {
      const phno = val;
      console.log(phno);
      const db = firebase.firestore();
      db.collection('UserProfile').doc(`${phno}`).get().then(doc => {
        console.log(doc.data());
        this.username = doc.data().name;
        this.image = doc.data().image;
      }).catch(err => {
        console.log(err);
      });
    });
  }

  edit() {
    this.router.navigate(['/editprofile']);
  }

}
