import { Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
let lastVisible: any;
const recentData = [];
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  location = 'Vishakapatnaam';
  db = firebase.firestore();
  last: any;
  items: any = [];
  imagesArr = [];
  constructor(
    private loadingController: LoadingController, private router: Router) {
    this.getList();
  }


  // ionViewWillEnter() {
  //   this.getList();
  // }

  async getList() {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
    const teamMembers = [];
    await this.db.collection('UserList').limit(8).get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        console.log(doc.id, ' => ', doc.data());
        teamMembers.push(doc.data());
      });
      lastVisible = querySnapshot.docs[querySnapshot.docs.length - 1];
    });
    this.loadingController.dismiss();
    this.last = lastVisible;
    this.items = teamMembers;
    this.loadingController.dismiss();
  }

  click(item: any) {
    console.log(item);
    this.imagesArr = item.image;
    const len = this.imagesArr.length;
    console.log(this.imagesArr);
    // tslint:disable-next-line: max-line-length
    this.router.navigate(['/itemview', { description: item.description, rate: item.rate, location: item.location, image1: this.imagesArr, imgLength: len, phonenumber: item.phonenumber }]);

  }
}

