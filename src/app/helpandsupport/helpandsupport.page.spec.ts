import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HelpandsupportPage } from './helpandsupport.page';

describe('HelpandsupportPage', () => {
  let component: HelpandsupportPage;
  let fixture: ComponentFixture<HelpandsupportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HelpandsupportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HelpandsupportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
