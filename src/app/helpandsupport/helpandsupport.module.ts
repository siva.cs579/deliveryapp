import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HelpandsupportPageRoutingModule } from './helpandsupport-routing.module';

import { HelpandsupportPage } from './helpandsupport.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HelpandsupportPageRoutingModule
  ],
  declarations: [HelpandsupportPage]
})
export class HelpandsupportPageModule {}
