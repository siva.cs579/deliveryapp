import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-helpandsupport',
  templateUrl: './helpandsupport.page.html',
  styleUrls: ['./helpandsupport.page.scss']
})
export class HelpandsupportPage implements OnInit {
  constructor(private Socialshare: SocialSharing) {}

  ngOnInit() {}

  shareinfo() {
    const text =
      'Please install the new OLX app and get new updates and new features';
    // tslint:disable-next-line: max-line-length
    const image =
      // tslint:disable-next-line: max-line-length
      'https://firebasestorage.googleapis.com/v0/b/star-9d810.appspot.com/o/unnamed.png?alt=media&token=2ce3a332-8885-4c49-9956-e14598f3b6d9';
    this.Socialshare.share(text, image)
      .then(res => {
        alert('success' + res);
      })
      .catch(err => {
        alert('error:- ' + err);
      });
  }
}
