import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HelpandsupportPage } from './helpandsupport.page';

const routes: Routes = [
  {
    path: '',
    component: HelpandsupportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HelpandsupportPageRoutingModule {}
