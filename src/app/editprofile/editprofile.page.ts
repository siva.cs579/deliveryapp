import { Component, OnInit } from '@angular/core';
import { SaveItemService } from '../Services/save-item.service';
import { CameraServiceService } from '../Services/camera-service.service';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.page.html',
  styleUrls: ['./editprofile.page.scss'],
})
export class EditprofilePage implements OnInit {

  itemImages: any;
  imgURL = 'assets/imgs/man.png';
  localStorage: Storage;
  editProfile: FormGroup;
  username: any;
  description: any;
  phonenumber: any;
  constructor(
    private router: Router, public camS: CameraServiceService, public saveItemDataS: SaveItemService,
    public loadingController: LoadingController,
    private storage: Storage,
    private fb: FormBuilder) {
    this.localStorage = this.storage;
  }

  ngOnInit() {
    this.editProfile = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      phonenumber: ['', Validators.required]
    });
    this.getProfiledata();
  }
  getProfiledata() {
    this.localStorage.get('phone').then(val => {
      const phno = val;
      console.log(phno);
      const db = firebase.firestore();
      db.collection('UserProfile').doc(`${phno}`).get().then(doc => {
        console.log(doc.data());
        this.username = doc.data().name;
        this.imgURL = doc.data().image;
        this.description = doc.data().description;
        this.phonenumber = doc.data().phonenumber;
      }).catch(err => {
        console.log(err);
      });
    });
  }

  openCam() {
    this.camS.selectImage().then(res => {
      // alert('cam res' + res + JSON.stringify(res))
      this.itemImages = res;
      // alert('event image array ' + this.itemImages)
      this.uploadPicture1(this.itemImages);
    });
  }

  uploadPicture1(data) {

    this.imgUpload(data);
  }

  async imgUpload(data) {
    const loading = await this.loadingController.create({
      message: 'Please wait...',
    });
    await loading.present();
    const storageRef = firebase.storage().ref('/Profile_images');
    // alert('store ref' + storageRef)
    const uuid = this.guid();
    const imageRef = storageRef.child('picmsg' + uuid);
    // alert('image ref' + imageRef)
    imageRef.putString(data, firebase.storage.StringFormat.DATA_URL)
      .then((snapshot) => {
        // alert('img 1')
        // var ref = imageRef.getDownloadURL()
        // alert('download url' + ref);
        // this.imgURL= ref;
        // alert(this.imgURL);
        // this.sendImgUrl(ref, data)
        imageRef.getDownloadURL().then((url) => {
          this.imgURL = url;
        }).catch((error) => {
          console.log(error);
        });

      }).catch((error) => {
        alert('Error Uploading I96mage: ' + JSON.stringify(error) + 'check ' + error);
      });
    this.loadingController.dismiss();

  }
  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
  }


  async saveItemData(data) {
    this.localStorage.get('phone').then(val => {
      const phno = val;
      const db = firebase.firestore();
      const docRef = db.collection('UserProfile').doc(`${phno}`);
      docRef.set({
        name: this.editProfile.controls.name.value,
        description: this.editProfile.controls.description.value,
        phonenumber: this.editProfile.controls.phonenumber.value,
        image: this.imgURL
      }).then((doc) => {
        console.log(doc);
        alert('Updated Successfully');
        this.router.navigateByUrl('/tabs/explore');
      });
    });
  }
}
